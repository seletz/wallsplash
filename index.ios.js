/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    ActivityIndicatorIOS,
    Dimensions,
    View,
    PanResponder,
    CameraRoll,
    AlertIOS
} from 'react-native';

var Swiper          = require("react-native-swiper");
var NetworkImage    = require('react-native-image-progress');
var Progress        = require('react-native-progress');
var ShakeEvent      = require('react-native-shake-event-ios');


var Utils           = require("./Utils.js");
var RandManager     = require("./RandManager.js");

// Components
var ProgressHUD     = require('./ProgressHUD.js');

var {width, height} = Dimensions.get('window');

const NUM_WALLPAPERS = 5;
const DOUBLE_TAP_DELAY = 300; // milliseconds
const DOUBLE_TAP_RADIUS = 20;

class SplashWalls extends Component {
    constructor(props) {
        super(props);

        this.state = {
            wallsJSON: [],
            isLoading: true,
            isHudVisible: false,
        };



        this.prevTouchInfo = {
            prevTouchX: 0,
            prevTouchY: 0,
            prevTouchTimeStamp: 0
        };

        this.currentWallIndex = 0;

    }

    componentWillMount = function() {
        this.imageiPanResponder = PanResponder.create({
            onStartShouldSetPanResponder: this.handleStartShouldSetPanResponder,
            onPanResponderGrant: this.handlePanResponderGrant,
            onPanResponderRelease: this.handlePanResponderEnd,
            onPanResponderTerminate: this.handlePanResponderEnd
        });

        ShakeEvent.addEventListener('shake', () => {
            this.initialize();
            this.fetchWallsJSON();
        });
    };

    initialize = function() {
        this.setState({
            wallsJSON: [],
            isLoading: true,
            isHudVisible: false
        });

        this.currentWallIndex = 0;
    };

    fetchWallsJSON() {
        console.log("Wallpapers will be really fetched.");
        var url = "http://unsplash.it/list";

        fetch(url)
            .then(response => response.json())
            .then(jsonData => {
                var randomIds = RandManager.uniqueRandomNumbers(NUM_WALLPAPERS, 0, jsonData.length);
                var walls = [];

                console.debug(randomIds);

                randomIds.forEach(randomId => {
                    walls.push(jsonData[randomId]);
                });

                console.debug(walls);

                this.setState({
                    isLoading: false,
                    wallsJSON: [].concat(walls)
                });
            })
            .catch(error => console.error("Fetch error", error));
    }

    saveCurrentWallpaperToCameraRoll() {
        var {wallsJSON} = this.state;
        var currentWall = wallsJSON[this.currentWallIndex];
        var currentWallURL = `http://unsplash.it/${currentWall.width}/${currentWall.height}?image=${currentWall.id}`;

        this.setState({isHudVisible: true});

        CameraRoll.saveImageWithTag(currentWallURL)
            .then((data) => {
                this.setState({isHudVisible: false});
                AlertIOS.alert(
                    'Saved',
                    'Wallpaper successfully saved to Camera Roll',
                    [
                        {text: 'High 5!', onPress: () => console.log('OK Pressed!')}
                    ]
                );
            })
            .catch(err => {
                this.setState({isHudVisible: false});
                console.error('Error saving to camera roll', err);
            })

    }

    componentDidMount() {
        this.fetchWallsJSON();

        ShakeEvent.addEventListener('shake', () => {
            this.initialize();
            this.fetchWallsJSON();
        });
    }

    render() {
        var {isLoading} = this.state;

        if (isLoading)
            return this.renderLoadingMessage();
        else
            return this.renderResults();
    }

    handleStartShouldSetPanResponder(e, gestureState) {
        return true;
    }

    isDoubleTap(currentTouchTimeStamp, {x0, y0}) {
        var {prevTouchX, prevTouchY, prevTouchTimeStamp} = this.prevTouchInfo;
        var dt = currentTouchTimeStamp - prevTouchTimeStamp;

        return (dt < DOUBLE_TAP_DELAY && Utils.distance(prevTouchX, prevTouchY, x0, y0) < DOUBLE_TAP_RADIUS);
    }

    handlePanResponderGrant = function(e, gestureState) {
        console.log('Finger touched the image');

        var currentTouchTimeStamp = Date.now();

        if( this.isDoubleTap(currentTouchTimeStamp, gestureState) )
            this.saveCurrentWallpaperToCameraRoll();

        this.prevTouchInfo = {
            prevTouchX: gestureState.x0,
            prevTouchY: gestureState.y0,
            prevTouchTimeStamp: currentTouchTimeStamp
        };
    }.bind(this);

    handlePanResponderEnd(e, gestureState) {
        console.log('Finger pulled up from the image');
    }

    onMomentumScrollEnd = function(e, state, context) {
        this.currentWallIndex = state.index;
    }.bind(this);

    renderLoadingMessage() {
        return (
            <View style={styles.loadingContainer}>
                <ActivityIndicatorIOS
                    animating={true}
                    color={'#fff'}
                    size={'small'}
                    style={{margin: 15}} />
                <Text style={{color: '#fff'}}>Contacting Unsplash</Text>
            </View>
        );
    }

    renderResults() {
        var {wallsJSON, isLoading, isHudVisible} = this.state;
        if (!isLoading) {
            return (
                <View>
                    <Swiper
                        index={this.currentWallIndex}
                        loop={false}
                        onMomentumScrollEnd={this.onMomentumScrollEnd} >
                        {
                            wallsJSON.map((wallpaper, index) => {
                                return (
                                    <View key={index}>
                                        <NetworkImage
                                            source={{uri: `https://unsplash.it/${wallpaper.width}/${wallpaper.height}?image=${wallpaper.id}`}}
                                            indicator={Progress.Circle}
                                            style={styles.wallpaperImage}
                                            {...this.imageiPanResponder.panHandlers}>
                                            <Text style={styles.label}>Photo by</Text>
                                            <Text style={styles.label_authorName}>{wallpaper.author}</Text>
                                        </NetworkImage>
                                    </View>
                                )
                            })
                        }
                    </Swiper>
                    <ProgressHUD width={width} height={height} isVisible={isHudVisible}/>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#333',
    },
    wallpaperImage: {
        flex: 1,
        width: width,
        height: height,
        backgroundColor: '#000'
    },
    label: {
        position: 'absolute',
        color: '#fff',
        fontSize: 13,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        padding: 2,
        paddingLeft: 5,
        top: 20,
        left: 20,
        width: width/2
    },
    label_authorName: {
        position: 'absolute',
        color: '#fff',
        fontSize: 15,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        padding: 2,
        paddingLeft: 5,
        top: 41,
        left: 20,
        fontWeight: 'bold',
        width: width/2
    }
});

AppRegistry.registerComponent('SplashWalls', () => SplashWalls);
